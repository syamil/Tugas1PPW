$(document).ready(function(){

	var imgsDir = "imgs/";
	var dafaultImgs = "default_image";
	var imgs = ["0","1","2","3","4","5","6","7","0","1","2","3","4","5","6","7"];
	var extension = ".png";
	var imgsContainerSize = 16;
	var countImgsOpen = 0;
	var prefImg;
	var prefImgIdOne = "init id";
	var prefImgIdTwo = "init id";
	var solvedImgId = [];
	var isStart = false;

	makeRandomArray();
	loadImgs();

	$(".imageContainer").click(function(){

		var currentId = $(this).attr("id");
		if(isStart && solvedImgId.indexOf(currentId) == -1){
			$(this).find("img").animate({
				height: '200px',
	            width: '200px'
			}, function(){
				if(prefImgIdOne !== null && currentId !== prefImgIdOne){
					countImgsOpen += 1;
					if(countImgsOpen == 2){
						prefImgIdTwo = currentId;
						if(prefImg === imgs[currentId]){
							imgsMatch();
						}else{
							imgsDontMatch();
						}
						countImgsOpen = 0;
					}else{
						prefImgIdOne = currentId;
						prefImg = imgs[currentId];
					}
				}
			});
		}

	});

	$("#startButton").click(function(){
		isStart = true;
		alert("START");
	});

	function imgsMatch(){
		alert("Berhasil");
		solvedImgId.push(prefImgIdOne);
		solvedImgId.push(prefImgIdTwo);
		prefImgIdOne = "dari ulang";
		prefImgIdTwo = "dari ulang";
	}

	function imgsDontMatch(){
		
		$("#"+prefImgIdOne).find("img").animate({
			height: '0px',
            width: '200px'
		});
		$("#"+prefImgIdTwo).find("img").animate({
			height: '200px',
            width: '0px'
		});
	}

	function getImg(img){
		return imgsDir+img+extension;
	}


	function makeRandomArray(){
		var randomIndex;
		var temptArray = [];
		while(imgs.length > 0){
			randomIndex = Math.floor(Math.random() * imgs.length);
			temptArray.push(imgs[randomIndex]);
			imgs.splice(randomIndex, 1);
		}
		imgs = temptArray;
	}

	function loadImgs(){
		for(i = 0 ; i < imgsContainerSize ; i++){
			var currentElementID = "#"+i;
			$(currentElementID+" img").attr("src",getImg(imgs[i]));
		}
	}

});
