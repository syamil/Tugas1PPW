$(document).ready(function(){

	var username = [];
	var password = [];
	var urlJson = "json/users.json";
	
	$.ajax({
		url: urlJson,
		beforeSend: function(data){
		    if (data.overrideMimeType)
		    {
		      data.overrideMimeType("application/json");
		    }
		},
		dataType: 'json',
		success: function(result){
			var users = result.users;
			for(i = 0 ; i < users.length ; i++){
				var currentUser = users[i];
				var currentUsername = currentUser.username;
				var currentPassword = currentUser.password;
				username.push(currentUsername);
				password.push(currentPassword);

			}
		}
	});

	$("#loginButton").click(function(){
		var usernameOfUser = $("#username").val();
		var passwordOfUser = $("#password").val();
		var indexOfUsername = username.indexOf(usernameOfUser);
		var indexOfPassword = password.indexOf(passwordOfUser);
		var isLoginSuccess = indexOfUsername !== -1 && indexOfPassword !== -1;
		if(isLoginSuccess){
			open("http://stackoverflow.com","_self");
		}else{
			alert("username or password is wrong, please try again");
		}
	});

});